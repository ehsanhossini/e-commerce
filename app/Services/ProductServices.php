<?php


namespace App\Services;

use App\Models\Product;
use App\Repositories\ProductRepository;

class ProductServices implements ProductRepository
{
    /**
     * @inheritDoc
     */
    public function ProductList()
    {
        return Product::all();
    }

    /**
     * @inheritDoc
     */
    public function SearchProduct(string $product_name = null, float $min_price = null, float $max_price = null)
    {
        return Product::where('name', 'LIKE', '%' . $product_name . '%')
        ->orwhereHas('productColor', function ($query) use ($min_price, $max_price) {
            $query->whereBetween('price', [$min_price, $max_price]);
        })->paginate(10);
    }

}
