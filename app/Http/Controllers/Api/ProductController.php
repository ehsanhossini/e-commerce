<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resources\ProductListResource;
use App\Http\Resources\Resources\ProductSearchResource;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
//    TODO: We should implement authentication using jwt or passport but according to your task i did not implement it
    /**
     * @var ProductRepository
     */
    protected $product;

    /**
     * ProductController constructor.
     * @param ProductRepository $product
     */
    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * product list
     */
    public function list()
    {
        $data =  $this->product->ProductList();
        return response()->json(ProductListResource::collection(collect($data)));
    }

    public function searchProduct(Request $request)
    {
        $product_name =$request->get('product_name');
        $max_price =$request->get('max_price');
        $min_price =$request->get('min_price');
        $data =  $this->product->SearchProduct($product_name,$min_price,$max_price);
        $data = ProductSearchResource::collection($data);
        return response()->json($data);
    }
}
