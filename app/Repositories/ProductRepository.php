<?php

namespace App\Repositories;

use App\Models\Product;

interface ProductRepository
{
    /**
     * @return mixed
     */
    public function ProductList();

    /**
     * @param string|null $product_name
     * @param float|null $min_price
     * @param float|null $max_price
     * @return mixed
     */
    public function SearchProduct(string $product_name = null, float $min_price = null, float $max_price = null);

}
