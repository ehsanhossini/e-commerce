<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductColor;
use Faker\Generator as Faker;

$factory->define(ProductColor::class, function (Faker $faker) {
    return [
        'product_id' => \App\Models\Product::all()->random()->id,
        'color' => $faker->colorName,
        'price' => $faker->randomNumber(2),
    ];
});
